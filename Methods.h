#ifndef METHODS_H
#define METHODS_H

#include <vector>
#include <ctime>

struct TaskParams
{
    double T, lambda1, lambda2, gamma, nu, k, c, ro, A1, A2, omega1, omega2, fi1, fi2;
};

typedef std::vector<long> TimeVector;

class TimeLoggerBase
{
public:
    virtual void clear(){}
    virtual void start(){}
    virtual void getResult(TimeVector& res){}
    virtual void logTime(){}
};

class TimeLogger: public TimeLoggerBase
{
public:
    virtual void clear();
    virtual void start();
    virtual void getResult(TimeVector& res);
    virtual void logTime();
private:
    TimeVector result;
//    time_t t;
    clock_t t;
};


void ExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2);
void NonExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2);
void WeightScheme(TaskParams &params, double sigma, int n, int m, double **u1, double **u2);

void ExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog);
void NonExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog);
void WeightScheme(TaskParams &params, double sigma, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog);

#endif // METHODS_H
