#-------------------------------------------------
#
# Project created by QtCreator 2011-12-11T21:09:12
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = ChemReact
TEMPLATE = app


SOURCES += main.cpp\
        uiwindow.cpp \
        plot.cpp \
    Methods.cpp

INCLUDEPATH += /usr/include/qwt5 \
               /usr/include/qwtplot3d \
               /usr/include/qwt \
               /usr/include/qwt-qt4 \
               /usr/include/qwtplot3d-qt4

#INCLUDEPATH += /usr/include/qwt-qt4
#LIBS += -L/usr/lib -lqwt-qt4

#LIBS += /usr/lib/libqwt.so \
#        /usr/lib/libqwtplot3d.so

LIBS += -L/usr/lib -lqwtplot3d-qt4 \
        -L/usr/lib -lqwt

QMAKE_CXXFLAGS += -std=c++0x

HEADERS  += uiwindow.h \
    plot.h \
    Methods.h

FORMS    += uiwindow.ui








