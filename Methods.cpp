#include <math.h>
#include "Methods.h"
#include <algorithm>

void ExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog)
{
    double lambda1 = params.lambda1;
    double lambda2 = params.lambda2;
    double gamma = params.gamma;
    double nu = params.nu;
    double k = params.k;
    double c = params.c;
    double ro = params.ro;
    double A1 = params.A1;
    double A2 = params.A2;
    double omega1 = params.omega1;
    double omega2 = params.omega2;
    double fi1 = params.fi1;
    double fi2 = params.fi2;
    double h = 1.0 / (n-1);
    double tau = params.T / (m-1);
    //fill first layer
    tlog.clear();
    tlog.start();
    for(int i=0;i<n;i++)
    {
        u1[0][i] = A1 * cos(M_PI*i*h*omega1) + fi1;
        u2[0][i] = A2 * cos(M_PI*i*h*omega2) + fi2;
    }
    tlog.logTime();
    for(int j=1;j<m;j++)
    {
        tlog.start();
        for(int i=1;i<n-1;i++)
        {
            u1[j][i] = (lambda1*(u1[j-1][i-1]-2*u1[j-1][i]+u1[j-1][i+1])/(h*h)-gamma*u1[j-1][i]+k*u1[j-1][i]*u1[j-1][i]/u2[j-1][i]+ro) * tau + u1[j-1][i];
            u2[j][i] = (lambda2*(u2[j-1][i-1]-2*u2[j-1][i]+u2[j-1][i+1])/(h*h)-nu*u2[j-1][i]+c*u1[j-1][i]*u1[j-1][i]) * tau +u2[j-1][i];
        }
        u1[j][0] = u1[j][1];
        u1[j][n-1] = u1[j][n-2];
        u2[j][0] = u2[j][1];
        u2[j][n-1] = u2[j][n-2];
        tlog.logTime();
    }
}

void NonExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog)
{
    double lambda1 = params.lambda1;
    double lambda2 = params.lambda2;
    double gamma = params.gamma;
    double nu = params.nu;
    double k = params.k;
    double c = params.c;
    double ro = params.ro;
    double A1 = params.A1;
    double A2 = params.A2;
    double omega1 = params.omega1;
    double omega2 = params.omega2;
    double fi1 = params.fi1;
    double fi2 = params.fi2;
    double h = 1.0 / (n-1);
    double tau = params.T / (m-1);
    //fill first layer
    tlog.clear();
    tlog.start();
    for(int i=0;i<n;i++)
    {
        u1[0][i] = A1 * cos(M_PI*i*h*omega1) + fi1;
        u2[0][i] = A2 * cos(M_PI*i*h*omega2) + fi2;
    }
    tlog.logTime();
    //process layer by layer
    double *alpha;
    double *beta;
    alpha = new double[n-1];
    beta = new double[n-1];

    //sweep coefficients
    double A = lambda1/(h*h);
    double B = lambda1/(h*h);
    double C;
    double fi;
    double kappa1 = 1, kappa2 = 1;
    double mu1 = 0, mu2 = 0;

    for(int j=1;j<m;j++)
    {
        //compute u1 on j layer
        //sweep method
        tlog.start();
        alpha[0] = kappa1;
        beta[0] = mu1;
        A = lambda1/(h*h);
        B = lambda1/(h*h);
        C = 2.0*lambda1/(h*h)+1.0/tau+gamma;
        for(int i=1;i<n-1;i++)
        {
            fi = k * u1[j-1][i] * u1[j-1][i] / u2[j-1][i] + ro + u1[j-1][i] / tau;
            alpha[i] = B / (C - A * alpha[i-1]);
            beta[i] = (fi+A*beta[i-1])/(C-A*alpha[i-1]);
        }
        u1[j][n-1] = (-kappa2*beta[n-2]-mu2)/(kappa2*alpha[n-2]-1);
        for(int i=n-2;i>=0;i--)
            u1[j][i]=alpha[i]*u1[j][i+1]+beta[i];

        //compute u2 on j layer
        A = lambda2/(h*h);
        B = lambda2/(h*h);
        C = 2.0*lambda2/(h*h)+1.0/tau+nu;
        //sweep method
        alpha[0] = kappa1;
        beta[0] = mu1;
        for(int i=1;i<n-1;i++)
        {
            fi = c*u1[j-1][i]*u1[j-1][i]+u2[j-1][i]/tau;
            alpha[i] = B/(C-A*alpha[i-1]);
            beta[i] = (fi+A*beta[i-1])/(C-A*alpha[i-1]);
        }
        u2[j][n-1] = (-kappa2*beta[n-2]-mu2)/(kappa2*alpha[n-2]-1);
        for(int i=n-2;i>=0;i--)
            u2[j][i]=alpha[i]*u2[j][i+1]+beta[i];
        tlog.logTime();
    }
    delete[] alpha;
    delete[] beta;
}


void WeightScheme(TaskParams &params, double sigma, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog)
{
    double lambda1 = params.lambda1;
    double lambda2 = params.lambda2;
    double gamma = params.gamma;
    double nu = params.nu;
    double k = params.k;
    double c = params.c;
    double ro = params.ro;
    double A1 = params.A1;
    double A2 = params.A2;
    double omega1 = params.omega1;
    double omega2 = params.omega2;
    double fi1 = params.fi1;
    double fi2 = params.fi2;
    double h = 1.0 / (n-1);
    double tau = params.T / (m-1);
    //fill first layer
    tlog.clear();
    tlog.start();
    for(int i=0;i<n;i++)
    {
        u1[0][i] = A1 * cos(M_PI*i*h*omega1) + fi1;
        u2[0][i] = A2 * cos(M_PI*i*h*omega2) + fi2;
    }
    tlog.logTime();
    //process layer by layer
    double *alpha;
    double *beta;
    alpha = new double[n-1];
    beta = new double[n-1];

    //sweep coefficients
    double A = (1-sigma)*lambda1/(h*h);
    double B = (1-sigma)*lambda1/(h*h);
    double C;
    double fi;
    double kappa1 = 1, kappa2 = 1;
    double mu1 = 0, mu2 = 0;

    for(int j=1;j<m;j++)
    {
        //compute u1 on j layer
        //sweep method
        tlog.start();
        alpha[0] = kappa1;
        beta[0] = mu1;
        A = (1-sigma)*lambda1/(h*h);
        B = (1-sigma)*lambda1/(h*h);
        C = (1-sigma)*2.0*lambda1/(h*h)+1.0/tau+gamma;
        for(int i=1;i<n-1;i++)
        {
            fi =  sigma*lambda1*(u1[j-1][i-1]-2*u1[j-1][i]+u1[j-1][i+1])/(h*h) + k * u1[j-1][i] * u1[j-1][i] / u2[j-1][i] + ro + u1[j-1][i] / tau;
            alpha[i] = B / (C - A * alpha[i-1]);
            beta[i] = (fi+A*beta[i-1])/(C-A*alpha[i-1]);
        }
        u1[j][n-1] = (-kappa2*beta[n-2]-mu2)/(kappa2*alpha[n-2]-1);
        for(int i=n-2;i>=0;i--)
            u1[j][i]=alpha[i]*u1[j][i+1]+beta[i];

        //compute u2 on j layer
        A = (1-sigma)*lambda2/(h*h);
        B = (1-sigma)*lambda2/(h*h);
        C = (1-sigma)*2.0*lambda2/(h*h)+1.0/tau+nu;
        //sweep method
        alpha[0] = kappa1;
        beta[0] = mu1;
        for(int i=1;i<n-1;i++)
        {
            fi = lambda2*sigma*(u2[j-1][i-1]-2*u2[j-1][i]+u2[j-1][i+1])/(h*h)+c*u1[j-1][i]*u1[j-1][i]+u2[j-1][i]/tau;
            alpha[i] = B/(C-A*alpha[i-1]);
            beta[i] = (fi+A*beta[i-1])/(C-A*alpha[i-1]);
        }
        u2[j][n-1] = (-kappa2*beta[n-2]-mu2)/(kappa2*alpha[n-2]-1);
        for(int i=n-2;i>=0;i--)
            u2[j][i]=alpha[i]*u2[j][i+1]+beta[i];
        tlog.logTime();
    }
    delete[] alpha;
    delete[] beta;
}

//void WeightScheme(TaskParams &params, double sigma, int n, int m, double **u1, double **u2,TimeLoggerBase& tlog)
//{
//    double lambda1 = params.lambda1;
//    double lambda2 = params.lambda2;
//    double gamma = params.gamma;
//    double nu = params.nu;
//    double k = params.k;
//    double c = params.c;
//    double ro = params.ro;
//    double A1 = params.A1;
//    double A2 = params.A2;
//    double omega1 = params.omega1;
//    double omega2 = params.omega2;
//    double fi1 = params.fi1;
//    double fi2 = params.fi2;
//    double h = 1.0 / (n-1);
//    double tau = params.T / (m-1);
//    //fill first layer
//    tlog.clear();
//    tlog.start();
//    for(int i=0;i<n;i++)
//    {
//        u1[0][i] = A1 * cos(M_PI*i*h*omega1) + fi1;
//        u2[0][i] = A2 * cos(M_PI*i*h*omega2) + fi2;
//    }
//    tlog.logTime();
//    //process layer by layer
//    double *alpha;
//    double *beta;
//    alpha = new double[n-1];
//    beta = new double[n-1];

//    //sweep coefficients
//    double A;
//    double B;
//    double C;
//    double fi;
//    double kappa1 = 1, kappa2 = 1;
//    double mu1 = 0, mu2 = 0;

//    for(int j=1;j<m;j++)
//    {
//        //compute u1 on j layer
//        //sweep method
//        tlog.start();
//        alpha[0] = kappa1;
//        beta[0] = mu1;
//        A = lambda1/(h*h)*(1.0 - sigma);
//        B = lambda1/(h*h)*(1.0 - sigma);
//        C = 2.0*lambda1/(h*h)*(1-sigma)+1.0/tau+gamma;
//        for(int i=1;i<n-1;i++)
//        {
//            fi = lambda1*sigma*(u1[j-1][i-1]-2.0*u1[j-1][i]+u1[j-1][i+1])/(h*h) + k * u1[j-1][i] * u1[j-1][i] / u2[j-1][i] + ro + u1[j-1][i] / tau;
//            alpha[i] = B / (C - A * alpha[i-1]);
//            beta[i] = (fi+A*beta[i-1])/(C-A*alpha[i-1]);
//        }
//        u1[j][n-1] = (-kappa2*beta[n-2]-mu2)/(kappa2*alpha[n-2]-1);
//        for(int i=n-2;i>=0;i--)
//            u1[j][i]=alpha[i]*u1[j][i+1]+beta[i];

//        //compute u2 on j layer
//        A = lambda2/(h*h)*(1.0 - sigma);
//        B = lambda2/(h*h)*(1.0 - sigma);
//        C = 2.0*lambda2/(h*h)*(1-sigma)+1.0/tau+nu;
//        //sweep method
//        alpha[0] = kappa1;
//        beta[0] = mu1;
//        for(int i=1;i<n-1;i++)
//        {
//            fi = lambda2*sigma*(u2[j-1][i-1]-2.0*u2[j-1][i]+u2[j-1][i+1])/(h*h)+c*u1[j-1][i]*u1[j-1][i]+u2[j-1][i]/tau;
//            alpha[i] = B/(C-A*alpha[i-1]);
//            beta[i] = (fi+A*beta[i-1])/(C-A*alpha[i-1]);
//        }
//        u2[j][n-1] = (-kappa2*beta[n-2]-mu2)/(kappa2*alpha[n-2]-1);
//        for(int i=n-2;i>=0;i--)
//            u2[j][i]=alpha[i]*u2[j][i+1]+beta[i];
//        tlog.logTime();
//    }
//    delete[] alpha;
//    delete[] beta;
//}

void TimeLogger::clear()
{
    this->result.clear();
}

void TimeLogger::start()
{
//    time(&t);
    t = clock();
}


void TimeLogger::getResult(TimeVector &res)
{
//    std::copy(result.begin(),result.end(),res.begin());
    for(int i = 0;i<result.size();++i)
    {
        res.push_back(result[i]);
    }
}

void TimeLogger::logTime()
{
    clock_t tmp = t;
//    time(&t);
    t = clock();
    result.push_back((long)(t-tmp)/10000);
}
//-----------------------------------
void ExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2)
{
    TimeLoggerBase tlogger;
    ExplicitScheme(params,n,m,u1,u2,tlogger);

}

void NonExplicitScheme(TaskParams &params, int n, int m, double **u1, double **u2)
{
    TimeLoggerBase tlogger;
    NonExplicitScheme(params,n,m,u1,u2,tlogger);
}

void WeightScheme(TaskParams &params, double sigma, int n, int m, double **u1, double **u2)
{
    TimeLoggerBase tlogger;
    WeightScheme(params,sigma,n,m,u1,u2,tlogger);
}
