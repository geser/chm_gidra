#ifndef UIWINDOW_H
#define UIWINDOW_H

#include <QMainWindow>
#include <qwt3d_surfaceplot.h>
#include <QtCore>
#include "plot.h"
#include <vector>

typedef std::vector<double> Vector;
typedef std::vector<Vector> Matrix;

namespace Ui {
    class UIWindow;
}

class UIWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit UIWindow(QWidget *parent = 0);
    Plot* plot;
    Plot* plot1;
    Matrix m_u1;
    Matrix m_u2;
    Vector m_x;
    ~UIWindow();

private slots:
    void on_pushButton_clicked();

    void on_spinBox_valueChanged(int arg1);
    void resizeEvent(QResizeEvent * e);


    void on_grid_clicked();
    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_2_clicked();

private:
    void grid();

private:
    Ui::UIWindow *ui;
    bool isGrid,isFloor,isDots;

};

#endif // UIWINDOW_H
