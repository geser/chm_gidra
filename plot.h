#ifndef PLOT_H
#define PLOT_H

#include <vector>
#include <math.h>
#include <qwt3d_surfaceplot.h>
#include <qwt3d_function.h>
#include <iostream>

typedef std::vector<std::vector<double> > Matrix;

using namespace Qwt3D;

class Plot : public SurfacePlot
{
    class Rosenbrock : public Function
    {


    public:
      Rosenbrock(SurfacePlot* pw)
          :Function(pw),parent(pw)
      {}

      double operator()(double x, double t)
      {
          double a = 0.0;
          double c = 0.0;
          int i = (x)/h;
          int j = (t)/tu;
          double x1=i*h;
          double x2=(i+1)*h;
          double t1=(j)*tu;
          double t2=(j+1)*tu;

          if(fabs(x1-x)>fabs(x2-x))
          {
              i++;
          }
          if(fabs(t1 -t)>fabs(t2 -t))
          {
              j++;
          }
          return u->at(j).at(i);
      }
      void setData(Matrix &_u, double _h, double _t,double maxT)
      {
          double a = 0.0;
          double c = 0.0;
          double b = 1.0;
          d = maxT;
          u = &_u;
          h = _h;
          tu = _t;
          this->setMesh(u->at(0).size(),u->size());
          std::cout<<"x dim: "<<u->at(0).size()<<std::endl;
          std::cout<<"t dim: "<<u->size()<<std::endl;
          this->setDomain(a,b,c,d);
          this->setMinZ(-10);

          //set drawing style
          this->create();
          parent->coordinates()->axes[X1].setLabelString("X");
          parent->coordinates()->axes[Y1].setLabelString("Y");
          parent->coordinates()->axes[Z1].setLabelString("Z");

          double majl = 0.25;
          parent->coordinates()->setTicLength(majl,0.6*majl);
          parent->coordinates()->setGridLinesColor(RGBA(0,0,0.5));
      }
    private:
      Matrix* u;
      double h,tu;
      double d;
      SurfacePlot* parent;
    };
public:
    Plot(QWidget* parent=0);
    void setData(Matrix& _u,double _h,double _t,double maxT);
    void setData(double** src, uint n, uint m,double maxT);
    void draw();
    void setGrid(SIDE s,bool b);
    void floor(bool f);
    void style(bool f);
private:
    Rosenbrock rosenbrock;
};

#endif // PLOT_H
