#include "plot.h"
#include <algorithm>



void Plot::setData(Matrix &_u, double _h, double _t,double maxT)
{
    rosenbrock.setData(_u,_h,_t,maxT);
}

#include <algorithm>
#include <iostream>
void Plot::setData(double **src, uint n, uint m,double maxT)
{
    Matrix matrix;
    std::vector<double> row;
    double _h = 1.0/(n-1);
    double _t = maxT/(m-1);

    for (uint i=0; i<m; i++)
    {
        row.clear();
        for(int j = 0;j<n;j++)
        {
            row.push_back(src[i][j]);
        }
        matrix.push_back(row);
    }

    setData(matrix, _h, _t,maxT);
}

Plot::Plot(QWidget* parent):SurfacePlot(parent), rosenbrock(this)
{
  setTitle("");
  for (unsigned i=0; i!=coordinates()->axes.size(); ++i)
  {
    coordinates()->axes[i].setMajors(7);
    coordinates()->axes[i].setMinors(4);
  }
}
void Plot::draw()
{
    setRotation(30,0,15);
    setScale(1,1,10);
    setShift(0.15,0,0);
    setZoom(0.9);

//    for (unsigned i=0; i!=coordinates()->axes.size(); ++i)
//    {
//      coordinates()->axes[i].setMajors(20);
//      coordinates()->axes[i].setMinors(10);
//    }


//    coordinates()->axes[X1].setLabelString("x-axis");
//    coordinates()->axes[Y1].setLabelString("y-axis");
//    coordinates()->axes[Z1].setLabelString(QChar (0x38f)); // Omega - see http://www.unicode.org/charts/

//    setCoordinateStyle(BOX);

    updateData();
    updateGL();
}

void Plot::setGrid(Qwt3D::SIDE s, bool b)
{
     int sum = coordinates()->grids();

      if (b)
        sum |= s;
      else
        sum &= ~s;

      coordinates()->setGridLines(sum!=Qwt3D::NOSIDEGRID, sum!=Qwt3D::NOSIDEGRID, sum);
      updateGL();
}

void Plot::floor(bool f)
{
    if(f)
    {
        setFloorStyle(FLOORISO);
    }
    else
    {
        setFloorStyle(NOFLOOR);
    }

    updateData();
    updateGL();
}

void Plot::style(bool f)
{
    if(f)
    {
        setPlotStyle(Qwt3D::POINTS);
    }
    else
    {
        setPlotStyle(FILLEDMESH);
    }
    updateData();
    updateGL();
}
