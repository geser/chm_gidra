#include "mesh.h"

Mesh::Mesh(SurfacePlot* sp):Function(sp)
{
    n=0;
    m=0;
    data=0;
}

/*Mesh::Mesh(int n, int m, double **v)
{
    m=0;
    n=0;
    data=0;
    SetData(n,m,v);
}*/

double Mesh::GetItem(int i, int j)
{
    return data[i][j];
}

void Mesh::SetData(int n, int m, double **v)
{
    if(data!=0 && this->m!=0)
    {
        for(int i=0;i<this->m;i++)
            delete[] data[i];
        delete[] data;
    }
    this->n=n;
    this->m=m;
    data = new double *[m];
    for(int i=0;i<m;i++)
        data[i]=new double[n];
    for(int i=0;i<m;i++)
        for(int j=0;j<n;j++)
            data[i][j]=v[i][j];
}

double *Mesh::operator [](int i)
{
    return data[i];
}

Mesh::~Mesh()
{
    if(data!=0)
    {
        for(int i=0;i<m;i++)
            delete[] data[i];
        delete[] data;
    }
}
