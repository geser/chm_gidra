#include "uiwindow.h"
#include "ui_uiwindow.h"
#include "Methods.h"

#include <qwt_plot_curve.h>
#include <QtGui>
#include <memory.h>

UIWindow::UIWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UIWindow),isGrid(false),isFloor(false),isDots(false)
{
    ui->setupUi(this);

    plot = new Plot(ui->tabVisualization);
    plot1 = new Plot(ui->tabVisualization);
    int w = ui->tabVisualization->width();
    int h = ui->tabVisualization->height();
    plot->resize(w/2,h);
    plot1->resize(w/2,h);
    plot1->move(w/2,0);
}

UIWindow::~UIWindow()
{
    delete ui;
}

void UIWindow::on_pushButton_clicked()
{
    int n = ui->sbN->value();
    int m = ui->sbM->value();
    double sigma;
    TaskParams params;
    params.lambda1 = ui->leLambda1->text().toDouble();
    params.lambda2 = ui->leLambda2->text().toDouble();
    params.nu = ui->leNu->text().toDouble();
    params.ro = ui->leRo->text().toDouble();
    params.c = ui->leC->text().toDouble();
    params.k = ui->leK->text().toDouble();
    params.A1 = ui->leA1->text().toDouble();
    params.A2 = ui->leA2->text().toDouble();
    params.gamma = ui->leGamma->text().toDouble();
    params.omega1 = ui->leOmega1->text().toDouble();
    params.omega2 = ui->leOmega2->text().toDouble();
    params.fi1 = ui->leFi1->text().toDouble();
    params.fi2 = ui->leFi2->text().toDouble();
    params.T = ui->leT->text().toDouble();
    sigma = ui->leSigma->text().toDouble();
    double **u1 = new double *[m+1];
    double **u2 = new double *[m+1];
    for(int i=0;i<m+1;i++)
    {
        u1[i]=new double[n+1];
        memset(u1[i],0,sizeof(double)*(n+1));
        u2[i]=new double[n+1];
        memset(u2[i],0,sizeof(double)*(n+1));
    }

    if(ui->rbExplicit->isChecked())
        ExplicitScheme(params, n, m, u1, u2);
    else
        if(ui->rbNonExplicit->isChecked())
            NonExplicitScheme(params,n,m,u1,u2);
        else
            WeightScheme(params, sigma, n, m, u1, u2);

    //output data to table
    QStringList horHeader;
    for(int i=0;i<n+1;i++)
        horHeader<<QString("%1").arg(i);
    QStringList vertHeader;
    for(int i=0;i<m+1;i++)
        vertHeader<<QString("%1").arg(i);
    ui->twU1->setRowCount(m);
    ui->twU2->setRowCount(m);
    ui->twU1->setColumnCount(n);
    ui->twU2->setColumnCount(n);
    ui->twU1->setHorizontalHeaderLabels(horHeader);
    ui->twU1->setVerticalHeaderLabels(vertHeader);
    ui->twU2->setHorizontalHeaderLabels(horHeader);
    ui->twU2->setVerticalHeaderLabels(vertHeader);
    QTableWidgetItem *tableItem1=0, *tableItem2=0;
    for(int j=0;j<m;j++)
    {
        for(int i=0;i<n;i++)
        {
            tableItem1 = new QTableWidgetItem(QString("%1").arg(u1[j][i]));
            tableItem2 = new QTableWidgetItem(QString("%1").arg(u2[j][i]));
            ui->twU1->setItem(j,i,tableItem1);
            ui->twU2->setItem(j,i,tableItem2);
        }
    }

    plot->setData(u1, n, m, params.T);
    plot1->setData(u2, n,m, params.T);

    Vector tmpRow;
    Vector tmpRow1;
    m_x.clear();
    double h = 1.0/(n-1);
    for(int i = 0;i<n;i++)
    {
        m_x.push_back(i*h);
    }
    m_u1.clear();
    m_u2.clear();
    for (uint i=0; i<m; i++)
    {
        tmpRow.clear();
        tmpRow1.clear();
        for(int j = 0;j<n;j++)
        {
            tmpRow.push_back(u1[i][j]);
            tmpRow1.push_back(u2[i][j]);
        }
        m_u1.push_back(tmpRow);
        m_u2.push_back(tmpRow1);
    }

    for(int i=0;i<m;i++)
    {
        delete[] u1[i];
        delete[] u2[i];
    }
    delete[] u1;
    delete[] u2;
}

void UIWindow::on_spinBox_valueChanged(int arg1)
{
    QwtPlotCurve* u1 = new QwtPlotCurve("curve1");
    QwtPlotCurve* u2 = new QwtPlotCurve("curve2");;
    QPen pen1(Qt::red);
    QPen pen2(Qt::blue);
    u1->setPen(pen1);
    u2->setPen(pen2);

    ui->qwtPlot->detachItems(QwtPlotCurve::Rtti_PlotCurve);

    u1->attach(ui->qwtPlot);
    u2->attach(ui->qwtPlot);
    int tmpInd = ui->spinBox->value();
    if(tmpInd<m_u1.size())
    {
        QwtCPointerData* dataU1 = new QwtCPointerData(const_cast<const double*>(m_x.data())
                                                            , const_cast<const double*>(m_u1.at(0).data()), m_x.size());
        u1->setData(dataU1);
//        u1->setData(&m_x[0],&m_u1.at(tmpInd).at(0),m_x.size());
//        u2->setData(&m_x[0],&m_u2.at(tmpInd).at(0),m_x.size());
    }
    ui->qwtPlot->replot();
}

void UIWindow::resizeEvent(QResizeEvent *e)
{
    ui->tabs->resize(e->size());
    int w = ui->tabs->width();
    int h = ui->tabs->height() - 30;
    plot->resize(w/2-1,h-50);
    plot1->resize(w/2-1,h-50);
    plot1->move(w/2,0);
    ui->twU1->resize(w/2-1,h);
    ui->twU2->resize(w/2-1,h);
    ui->twU2->move(w/2,0);
    ui->qwtPlot->resize(w-1,h - 50);

    ui->label_20->move(20,h-30);
    ui->spinBox->move(20+52,h-30);

    ui->pushButton_3->move(ui->pushButton_3->geometry().left(),h-30);
    ui->pushButton_4->move(ui->pushButton_4->geometry().left(),h-30);
    ui->grid->move(ui->grid->geometry().left(),h-30);
}

void UIWindow::grid()
{
    isGrid = !isGrid;
    plot->setGrid(Qwt3D::LEFT,isGrid);
    plot->setGrid(Qwt3D::RIGHT,isGrid);
    plot->setGrid(Qwt3D::FRONT,isGrid);
    plot->setGrid(Qwt3D::BACK,isGrid);
    plot1->setGrid(Qwt3D::LEFT,isGrid);
    plot1->setGrid(Qwt3D::RIGHT,isGrid);
    plot1->setGrid(Qwt3D::FRONT,isGrid);
    plot1->setGrid(Qwt3D::BACK,isGrid);
}


void UIWindow::on_grid_clicked()
{

    grid();
}

void UIWindow::on_pushButton_3_clicked()
{
    isFloor = !isFloor;
    plot->floor(isFloor);
    plot1->floor(isFloor);
}

void UIWindow::on_pushButton_4_clicked()
{
    isDots = !isDots;
    plot->style(isDots);
    plot1->style(isDots);
}

void UIWindow::on_pushButton_2_clicked()
{
    int n = ui->sbN_2->value();
    int m = ui->sbM_2->value();
    double sigma;
    TaskParams params;
    params.lambda1 = ui->leLambda1_2->text().toDouble();
    params.lambda2 = ui->leLambda2_2->text().toDouble();
    params.nu = ui->leNu_2->text().toDouble();
    params.ro = ui->leRo_2->text().toDouble();
    params.c = ui->leC_2->text().toDouble();
    params.k = ui->leK_2->text().toDouble();
    params.A1 = ui->leA1_2->text().toDouble();
    params.A2 = ui->leA2_2->text().toDouble();
    params.gamma = ui->leGamma_2->text().toDouble();
    params.omega1 = ui->leOmega1_2->text().toDouble();
    params.omega2 = ui->leOmega2_2->text().toDouble();
    params.fi1 = ui->leFi1_2->text().toDouble();
    params.fi2 = ui->leFi2_2->text().toDouble();
    params.T = ui->leT_2->text().toDouble();
    sigma = ui->leSigma_2->text().toDouble();
    //first method
    double **u1_1 = new double *[m+1];
    double **u2_1 = new double *[m+1];
    for(int i=0;i<m+1;i++)
    {
        u1_1[i]=new double[n+1];
        memset(u1_1[i],0,sizeof(double)*(n+1));
        u2_1[i]=new double[n+1];
        memset(u2_1[i],0,sizeof(double)*(n+1));
    }
    //second method
    double **u1_2 = new double *[m+1];
    double **u2_2 = new double *[m+1];
    for(int i=0;i<m+1;i++)
    {
        u1_2[i]=new double[n+1];
        memset(u1_2[i],0,sizeof(double)*(n+1));
        u2_2[i]=new double[n+1];
        memset(u2_2[i],0,sizeof(double)*(n+1));
    }

    TimeLogger tl1;
    TimeLogger tl2;
    //run first method
    if(ui->rbExplicit_2->isChecked())
    {
        ExplicitScheme(params, n, m, u1_1, u2_1,tl1);
    }
    else
    {
        if(ui->rbNonExplicit_2->isChecked())
        {
            NonExplicitScheme(params,n,m,u1_1,u2_1,tl1);
        }
        else
        {
            WeightScheme(params, sigma, n, m, u1_1, u2_1,tl1);
        }
    }

    //run second method
    if(ui->rbExplicit_3->isChecked())
    {
        ExplicitScheme(params, n, m, u1_2, u2_2,tl2);
    }
    else
    {
        if(ui->rbNonExplicit_3->isChecked())
        {
            NonExplicitScheme(params,n,m,u1_2,u2_2,tl2);
        }
        else
        {
            WeightScheme(params, sigma, n, m, u1_2, u2_2,tl2);
        }
    }
    TimeVector timeRes1;
    TimeVector timeRes2;
    tl1.getResult(timeRes1);
    tl2.getResult(timeRes2);
    //output data to table
    QStringList horHeader;
    for(int i=0;i<2;i++)
        horHeader<<QString("sch - %1").arg(i);
    QStringList vertHeader;
    for(int i=0;i<timeRes1.size();i++)
        vertHeader<<QString("t - %1").arg(i);
    ui->twU1_2->setRowCount(timeRes1.size());
    ui->twU1_2->setColumnCount(2);
    ui->twU1_2->setHorizontalHeaderLabels(horHeader);
    ui->twU1_2->setVerticalHeaderLabels(vertHeader);
    QTableWidgetItem *tableItem1=0;
    QTableWidgetItem *tableItem2=0;
    for(int j=0;j<timeRes1.size();j++)
    {
//        for(int i=0;i<n;i++)
//        {
            tableItem1 = new QTableWidgetItem(QString("%1").arg(timeRes1.at(j)));
            ui->twU1_2->setItem(j,0,tableItem1);

            tableItem2 = new QTableWidgetItem(QString("%1").arg(timeRes2.at(j)));
            ui->twU1_2->setItem(j,1,tableItem2);
//        }
    }


    for(int i=0;i<m;i++)
    {
        delete[] u1_1[i];
        delete[] u2_1[i];
    }
    delete[] u1_1;
    delete[] u2_1;

    for(int i=0;i<m;i++)
    {
        delete[] u1_2[i];
        delete[] u2_2[i];
    }
    delete[] u1_2;
    delete[] u2_2;
}
