#ifndef MESH_H
#define MESH_H

#include <qwt3d_function.h>

using namespace  Qwt3D;

class Mesh:public Function
{

private:
    int n, m;
    double **data;

public:
    Mesh(SurfacePlot* sp);
//    Mesh(int n, int m);
//    Mesh(int n, int m, double **v);
    void SetData(int n, int m, double **v);
    double GetItem(int i, int j);
    double *operator[](int i);
    double operator()(double x, double y);
    ~Mesh();

};

#endif // MESH_H
